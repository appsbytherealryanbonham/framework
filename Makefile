#
# Makefile
#
# You can run it from command line by typing eg.:
#   make review
#
# You can read an user friendly introduction to Makefile basics here:
# https://gist.github.com/Isinlor/035399fe952f5e3ced4280a5cc635a84
#

# provides an automatic review of the code
# review: phplint phpcpd phpmd phpstan psalm phpcs
# review:  phpcs
# checks syntax of source files and spec files
# phplint: src
# 	php -l ./src

# runs copy paste detection
# phpcpd: install
# 	bin/phpcpd ./src --min-lines=1 --min-tokens=35

# Run PHPUnit Test
build: install-dist

bundle: build
	mkdir -p dist
	cd src; zip -r ../dist/framework.zip .
test: install
	src/vendor/bin/phpunit -v ./src/tests
	
# runs PHP Code Sniffer
phpcs: install
	src/vendor/bin/phpcs -p -v \
             --report=checkstyle \
             --report-file=checkstyle.xml \
			 --runtime-set ignore_warnings_on_exit 1

# runs PHP Code Sniffer automatic fixer
phpcs-fix: install
	src/vendor/bin/phpcbf -p -v \
		--runtime-set ignore_warnings_on_exit 1

php-cs-fixer: install
	src/vendor/bin/php-cs-fixer fix -v
	
php-cs-fixer-check: install
	src/vendor/bin/php-cs-fixer fix --dry-run --using-cache=no
	
# runs PHP Mess Detector
phpmd: install
	src/vendor/bin/phpmd ./src text ruleset.xml

# runs PHPStan
phpstan: install
	src/vendor/bin/phpstan analyse ./src --level=max --no-progress

# runs Psalm
psalm: install
	src/vendor/bin/psalm

install-dist: composer vendor/autoload.php  yarn
	php composer.phar install --no-dev --quiet
	
# install dependencies allowing algorithm to run
install: composer vendor/autoload.php yarn

review: php-cs-fixer-check

# install dependencies trough the composer
vendor/autoload.php:
	php composer.phar install

composer: 
	# Install composer dependencies
	wget https://composer.github.io/installer.sig -O - -q | tr -d '\n' > installer.sig
	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
	php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
	php composer-setup.php
	php -r "unlink('composer-setup.php'); unlink('installer.sig');"

yarn:
	yarn install

start-docker: install
	docker-compose up