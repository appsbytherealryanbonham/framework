<?php

/**
 * Framework Example Addon For Open-Realty
 *
 *
 * Version 3.0  - 12/10/2021
 *  - Open source license
 *
 *
 * Version 2.2
 * - released at Dec,15/2010
 * - fixed $login::loginCheck calls
 * - improved the code and tested with Open-Realty® v.3.0.12
 *
 */

/**
 * install_addon()
 * This function should create and update any necessary database tables.
 * Developers can store version information for the add-on in the "addons" table
 * and use it to determine if an install or upgrade is needed.
 * @return
 */
function framework_install_addon()
{
    global $config, $conn;
    require_once($config['basepath'] . '/include/login.inc.php');
    $login = new login();
    $security = $login->loginCheck('Admin', true);
    if ($security !== true) {
        // DO NOTHING, NOT ADMIN LOGGED
    } else {
        $addon_name = 'framework';
        $current_version = '3.0';
        require_once($config['basepath'] . '/include/misc.inc.php');
        $misc = new misc();
        $sql = 'SELECT addons_version FROM ' . $config['table_prefix_no_lang'] . 'addons WHERE addons_name = "' . $addon_name . '"';
        $recordSet = $conn->Execute($sql);
        $addon_version = trim($recordSet->fields['addons_version']);
        if ($addon_version == '') {
            $sql = 'INSERT INTO ' . $config['table_prefix_no_lang'] . 'addons (addons_version, addons_name) VALUES ("' . $current_version . '", "' . $addon_name . '")';
            $recordSet = $conn->Execute($sql);
            $misc->log_action(strtoupper($addon_name) . ' add-on v.' . $current_version . ' installed.');
            return true;
        } elseif ($addon_version != $current_version) {
            switch ($addon_version) {
                case '1':
                case '2':
                case '2.1':
                case '2.2':
                case '2.3':
                default:
                    $sql = 'UPDATE ' . $config['table_prefix_no_lang'] . 'addons SET addons_version = "' . $current_version . '" WHERE addons_name = "' . $addon_name . '"';
                    $recordSet = $conn->Execute($sql);
                    $misc->log_action(strtoupper($addon_name) . ' add-on updated to v.' . $current_version);
                    break;
            }
            return true;
        }
        return false;
    }
}

/**
 * show_admin_icons()
 * This function should return an array of the html links that should be shown
 * on the administrative page.
 * @return array should return each link that shoudl be shown in the admin
 * section. Should return a string if it is a single link, or an array for
 * multiple links.
 */
function framework_show_admin_icons()
{
    $admin_link = '<a href="index.php?action=addon_framework_admin" title="Framework Add-on">Framework Add-on</a>';
    return $admin_link;
}

/**
 * load_template()
 * This should return an array with all the template tags for Open-Realty's
 * template engine to parse.
 * @return array List of template_fields
 */
function framework_load_template()
{
    $template_array = array('addon_framework_link');
    return $template_array;
}

/**
 * run_action_user_template()
 * This function handles user $_GET[] actions related to the add-on. Function
 * must be named using this method: addon_addonname_description.
 * @return string Should return all contents that should be displayed when an
 * add-on specific $_GET['action'] is called.
 */
function framework_run_action_user_template()
{
    switch ($_GET['action']) {
        case 'addon_framework_showpage1':
            $data = framework_display_addon_page();
            break;
        default:
            $data = '';
        break;
    }
    return $data;
}

/**
 * run_action_admin_template()
 * This function handles administrative $_GET[] actions related to the add-on.
 * Function must be named using this method: addon_addonname_description.
 * @return string Should return all contents that should be displayed when a
 * add-on specific $_GET['action'] is called.
 */
function framework_run_action_admin_template()
{
    switch ($_GET['action']) {
        case 'addon_framework_admin':
            $data = framework_display_admin_page();
            break;
        default:
            $data = '';
        break;
    }
    return $data;
}

/**
 * run_template_user_fields()
 * This function handles all the replacement of {template_tags} with the actual
 * content. All tags setup here must also be added to teh load_template function
 * in order for open-realty to parse them.
 * @param string $tag
 * @return string Should return all contents that should be displayed when an
 * add-on specific $tag is called.
 */
function framework_run_template_user_fields($tag = '')
{
    switch ($tag) {
        case 'addon_framework_link':
            $data = framework_display_addon_link();
            break;
        default:
            $data = '';
        break;
    }
    return $data;
}

/**
 * addonmanager_help()
 * This function provides a link to the documentation for the add-on, provides a
 * list of the add-on's template tags and a list of the add-on's action URL's.
 * @return array should return an array of the following values: TEMPLATE TAGS,
 * ACTION URLS, DOCUMENTATION URL - The template tags and action urls are to be
 * arrays where the array key is the template tag/action url and the value is
 * the description of the template tag/action url.
 */
function framework_addonmanager_help()
{
    $template_tags = array();
    $action_urls = array();
    $doc_url = 'http://wiki.open-realty.org/Addon_Documentation';
    $template_tags['addon_framework_link'] = 'Main Template File // Printer Friendly - This tag will display the link to the Framework Add-on Page.';
    $action_urls['addon_framework_showpage1'] = 'Public Site – This action will display the Add-on\'s demo page.';
    $action_urls['addon_framework_admin'] = 'Admin Site – This action will display the Add-on\'s admin page.';
    return array($template_tags,$action_urls,$doc_url);
}

/**
 * uninstall_tables()
 * This function runs the SQL commands to delete any tables that were added
 * during the installation of the add-on.
 * @return should return TRUE to indicate the add-on's tables were removed
 * properly.
 */
function framework_uninstall_tables()
{
    global $config, $conn;
    require_once($config['basepath'] . '/include/login.inc.php');
    $login = new login();
    $security = $login->loginCheck('Admin', true);
    if ($security !== true) {
        // DO NOTHING, NOT ADMIN LOGGED
    } else {
        require_once($config['basepath'] . '/include/misc.inc.php');
        $misc = new misc();
        $sql_uninstall[] = 'DELETE FROM ' . $config['table_prefix_no_lang'] . 'addons WHERE addons_name = "framework"';
        foreach ($sql_uninstall as $elementContents) {
            $recordSet = $conn->Execute($elementContents);
            if ($recordSet === false) {
                echo '<p style="font-weight:bold; color:red;">ERROR - ' . $elementContents . '</p>';
                return false;
            } else {
                $misc->log_action('Framework add-on successfully deleted.');
                return true;
            }
        }
    }
}
/**
 * This function handles the add-on's $_GET[] actions for calls to the ajax.php file from the backend of Open-Realty.
 * Each get action should have the function to be called defined.
 * The action must be named using this method: addon_name_description.
 * Here is an example of proper code for this function:
 */
function framework_run_action_ajax()
{
    switch ($_GET['action']) {
        case 'addon_framework_get_admin_ajax':
            $data = framework_get_admin_ajax();
            break;
        default:
            $data = '';
            break;
    } // End switch ($_GET['action'])
    return $data;
}
/**
* This function handles the add-on's $_GET[] actions for calls to the ajax.php file from the frontend of Open-Realty.
* Each get action should have the function to be called defined.
* The action must be named using this method: addon_name_description.
* Here is an example of proper code for this function:
*/
function framework_run_user_action_ajax()
{
    switch ($_GET['action']) {
        case 'addon_framework_get_user_ajax':
            $data = framework_get_user_ajax();
            break;
        default:
            $data = '';
        break;
    } // End switch ($_GET['action'])
    return $data;
}
// Add-on Specific Function
function framework_display_addon_link()
{
    $display = '<a href="index.php?action=addon_framework_showpage1" title="Framework Test">Framework Link</a>';
    return $display;
}

// Add-on Specific Function
function framework_display_addon_page()
{
    global $api;
    $data = $api->load_local_api('media__read', array('media_type'=>'listingsimages','media_parent_id'=>1,'media_output'=>'URL'));
    print_r($data);
}

// Add-on Specific Function
function framework_display_admin_page()
{
    global $api;
    $display = 'This is an Add-on page (replaced by the add-on tag {addon_framework_admin}).';
    //Do a simple API Call to get the number of listings in this site.
    $api_result = $api->load_local_api('listing__search', array('parameters'=>array(),'limit'=>0,'offset'=>0,'count_only'=>1));
    //API_DEBUG - To see the full results of what the API returned to you, you can uncomment the line below.
    //echo '<pre>'.print_r($result,TRUE)."</pre>\r\n";

    //Make sure there was no error in the API call.
    if ($api_result['error']) {
        //If an error occurs die and show the error msg;
        die($api_result['error_msg']);
    } else {
        $num_listing =  $api_result['listing_count'];
        $display .= '<br /> This site has '.$num_listing.' listings';
    }

    //No lets add some code to the page to do an admin side ajax call.
    $display .= '<br /><span class="ajaxmessage"></span><script type="text/javascript">
	$(document).ready(function() {
		$.get("{baseurl}/admin/ajax.php?action=addon_framework_get_admin_ajax",
			function(data){
				if(!data.error){
					$(".ajaxmessage").html(data.message);
				}else{
					$(".ajaxmessage").html("Our Ajax call failed");
				}
			},
			"json");
	});
	</script>';

    return $display;
}
//Add-on Specific Function
function framework_get_admin_ajax()
{
    //Seem I am using json in this example, i am setting the content-type header to json.
    header('Content-type: application/json');
    $array = array('error'=>false,'message'=>'This is our ajax response for our backend');
    return json_encode($array);
}
//Add-on Specific Function
function framework_get_user_ajax()
{
    header('Content-type: application/json');
    //Seem I am using json in this example, i am setting the content-type header to json.
    $array = array('error'=>false,'message'=>'This is our ajax response for our frontend');
    return json_encode($array);
}
